.. Starbase documentation master file, created by
   sphinx-quickstar on Fri Aug 11 21:52:37 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

====================================
Starbase 我支持你!  <3  Starbase, I will be there for u always & forever!
====================================


**标签:** Starbase | Starbase基于区块链众筹平台 | Tomoaki Sato  | Yu Yamanaka  | starbase.co 



----------------------
1.介绍(Introduce):
----------------------


    Starbase基于区块链众筹平台, Starbase is where everyone easy to challenge ~ by crowdfunding and crowdsourcing using blockchain tokens ~。官网请移步: https://starbase.co


     .. image:: ./_static/1.png

    特性：

     .. image:: ./_static/2.jpeg

    Starbase 在 Slack：
    
     .. image:: ./_static/3.jpeg
  


----------------------
2.我在行动(I am  in action):
----------------------

#a
    ---我在著名程序员博客ITEYE发表Starbase推文信息. I post starbase info via famous it blog (ITEYE) in China

    --- http://ironurbane.iteye.com/blog/2389425 

    --- 截图(ScreenShot)：

    .. image:: ./_static/4.jpeg



#b
    ---我在著名程序员博客开源中囯社区博客发表Starbase推文信息. I post starbase info via famous it blog（OSCHINA）in China

    --- https://my.oschina.net/u/2395742/blog/1506856

    --- 截图(ScreenShot)：

    .. image:: ./_static/5.jpeg



#c
    ---我在著名社交网站Twitter发表Starbase推文信息.  I post starbase info via famous  Society Media Website  (Twitter) 

    --- https://twitter.com/SteveYuanchunLi

    --- 截图(ScreenShot)：

    .. image:: ./_static/6.jpeg

